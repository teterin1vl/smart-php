<?php

define('DATA_FILE', 'data.json');

function connect() {
	$db = mysqli_connect("localhost", "root", "Smart123", "smart");
	return $db;
}

function close_connection($db) {
	mysqli_close($db);
}

function database_query($query) {
	$db = connect();
	$result = mysqli_query($db, $query);
	close_connection($db);
	return $result;
}

function getMessages() {
	$result = database_query("SELECT * FROM smart_message");
	$messages = [];
	while ($message = mysqli_fetch_assoc($result)) {
		$messages[] = $message;
	}
	return $messages;
}

function addMessage($text, $name = 'Igor') {
	$text = htmlspecialchars($text);
	database_query("INSERT INTO smart_message(name, text) VALUES ('{$name}', '{$text}')");
}

function deleteMessage($id) {
	database_query("DELETE FROM smart_message WHERE id = {$id}");
}

function updateMessage($id, $text) {
	database_query("UPDATE smart_message SET text = '{$text}' WHERE id = {$id}");
}