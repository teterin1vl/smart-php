<?php include('functions.php'); ?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'delete') {
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			deleteMessage($id);
			header("Location: /");
		}
	}
?>

<?php include('components/header.php') ?>

	<?php include('components/navbar.php') ?>

	<?php
		if (isset($_POST['text']) && $_POST['text']) {
			if (isset($_POST['name']) && $_POST['name']) {
				addMessage($_POST['text'], $_POST['name']);
			} else {
				addMessage($_POST['text']);
			}
		}
	?>

	<div class="container mt-3">
		<h3 class="mb-4 text-center">Smart forum</h3>
		<?php
			$messages = getMessages();

			foreach ($messages as $key => $message):
				switch ($message['name']) {
					case explode('.', $_SERVER['HTTP_HOST'])[0]:
						$class = "success text-right";
						break;
					default:
						$class = "warning";
						break;
				} ?>

				<?php if ($key == 0 || $message['name'] != $messages[$key - 1]['name']): ?>
					<div class="text-center">
						<small class='text-secondary'><?= date('j M H:i', strtotime($message['date'])) ?></small>
					</div>
				<?php endif; ?>
				<div class='message alert alert-<?= $class ?>'>
					<b><?= $message['name'] ?>:</b> <?= $message['text'] ?> <a href="/?action=delete&id=<?= $message['id'] ?>">X</a>
					<br>
				</div>

		<?php endforeach; ?>

		<form method="POST">
			<input class="form-control" type="text" name="name" hidden value="<?= explode('.', $_SERVER['HTTP_HOST'])[0] ?>">
			<textarea name="text" class="form-control"></textarea>
			<button class="btn btn-success" type="submit">Отправить</button>
		</form>

	</div>

<?php include('components/footer.php') ?>